/**
*  DMET 901 - Computer Vision
*  main.cpp
*  Assignment 1
*  Egyptian Licence Plates Regulation System
*
*  @author Joseph S. William 28-3921 T13
*/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <iostream>
#include <vector>
#include <stdio.h>

using namespace cv;
using namespace std;

//Methods Definitions
Mat loadImage( string imageName, int imageMode );
void showImage( string windowName, Mat image );
Vec3b saturatePixel(Vec3b colorPixel);
void addBightness(int value, string imageName);
void toBinary(string imageName);
Mat resizeImage(string imageName, int width, int height, bool isColor);
void blendImages(string imageName1, string imageName2, double ratio, bool interpolated, bool isColor);
void affineFrontView(string imageName, float Kx, float Ky);
void homographyFrontView(string imageName, bool isColor);
void mouseHandler(int event, int x, int y, int flags, void* param);
void homographyFrontView();

//Global Variables
int COLOR_IMAGE = CV_LOAD_IMAGE_COLOR;
int GRAYSCALE_IMAGE = CV_LOAD_IMAGE_GRAYSCALE;
int pointsCount = 0;
Point2f inputQuad[4];
Point2f outputQuad[4];

/**
* Run each Question seperatly by uncommenting its call in the main
*
* @param unused arguments.
* @return 0.
*/
int main(int argc, char const *argv[])
{
  /********************************************//**
  *  ... Question 1
  ***********************************************/
  //toBinary("L1.jpg");

  /********************************************//**
  *  ... Question 2
  ***********************************************/
  blendImages("L1.jpg", "logo.jpg", 0.8, false, false); // Implemented resize and grayscale
  //blendImages("L1.jpg", "logo.jpg", 0.8, false, true); // Implemented resize and color
  //blendImages("L1.jpg", "logo.jpg", 0.8, true, false); //Predifined resize and grayscale
  //blendImages("L1.jpg", "logo.jpg", 0.8, true, true); // Predifined resize and color

  /********************************************//**
  *  ... Question 3
  ***********************************************/
  //addBightness(50, "L2.jpg");

  /********************************************//**
  *  ... Question 4
  ***********************************************/
  //affineFrontView("L3.jpg", 0.1, 0.12);
  //affineFrontView("newL3.jpg", -0.12, 0);

  /********************************************//**
  *  ... Question 5
  ***********************************************/
  //homographyFrontView();

  waitKey(0);
  return 0;
}

/**
* Loads image from name
*
* @param the name of the image file.
* @return loaded image.
*/
Mat loadImage( string imageName, int imageMode )
{
  Mat image;
  image = imread( imageName, imageMode );

  if(! image.data )
  {
    cout <<  "Could not open or find the image" << std::endl ;
    return image;
  }

  return image;
}

/**
* Displays image from image matrix.
*
* @param name of the display window and the image matrix.
*/
void showImage( string windowName, Mat image )
{
  namedWindow( windowName , WINDOW_AUTOSIZE );
  imshow( windowName , image );
}

/**
* Checks the RBG color components of a pixel and saturates them to 255 if grater
* than 255.
*
* @param a vector of the color components.
* @return a vector of the saturated color components.
*/
Vec3b saturatePixel(Vec3b colorPixel)
{
  if (colorPixel[0] > 255) {
    colorPixel[0] = 255;
  }
  if (colorPixel[1] > 255) {
    colorPixel[1] = 255;
  }
  if (colorPixel[2] > 255) {
    colorPixel[2] = 255;
  }
  return colorPixel;
}

/**
* Mouse handler to get the coordinate of a pixel when clicked on.
*
* @param the click event, x and y locations, additional flags and a params method.
*/
void mouseHandler(int event, int x, int y, int flags, void* param)
{
  if( event != CV_EVENT_LBUTTONDOWN )
  return;

  Point2f pt = Point2f(x,y);
  inputQuad[pointsCount] = pt;
  std::cout << pt << std::endl;
  pointsCount++;

  if(pointsCount == 4)
  {
    homographyFrontView("L4.jpg", true);
    pointsCount = 0;
  }
}

/**
* Regiters the mouse function.
* Waits the user to select the 4 corners of the part of the images to get its frontview.
*/
void homographyFrontView() {
  std::cout << "pick the four corner pixels of the plate in the order: top left - top right - bottom right - bottom left " << std::endl;
  Mat m = loadImage("L4.jpg", COLOR_IMAGE);
  showImage("L4 - Original", m);
  int mouseParam= CV_EVENT_FLAG_LBUTTON;
  cvSetMouseCallback("L4 - Original", mouseHandler, &mouseParam);
}

/**
* Adds a brightness value to every color channel on an Image.
*
* @param value of the brightness and the name of the image.
*/
void addBightness(int value, string imageName)
{
  Mat image = loadImage( imageName, COLOR_IMAGE );
  for (int i = 0; i < image.cols; i++)
  {
    for (int j = 0; j < image.rows; j++)
    {
      Vec3b color = image.at<Vec3b>(Point(i,j));

      color[0] += value;
      color[1] += value;
      color[2] += value;

      image.at<Vec3b>(Point(i,j)) = saturatePixel(color);
    }
  }
  showImage("Bright Image", image);
}

/**
* Converts image to grayscale then into binary shY thresholding at pixel value 127.
*
* @param the name of the image to be converted to binary.
*/
void toBinary(string imageName)
{
  Mat image = loadImage( imageName, GRAYSCALE_IMAGE );
  for (int i = 0; i < image.cols; i++)
  {
    for (int j = 0; j < image.rows; j++)
    {
      Scalar intensity = image.at<uchar>(Point(i, j));
      if (intensity.val[0] > 127) {
        image.at<uchar>(Point(i, j)) = 255;
      }
      else
      {
        image.at<uchar>(Point(i, j)) = 0;
      }
    }
  }
  showImage("Threshold Image", image);
}

/**
* Resizes an image to to a given width and height - No interpolation applied.
*
*@param  the name of the image to be resized, the width and height of the output
*        image and the color mode.
*/
Mat resizeImage(string imageName, int width, int height, bool isColor)
{
  Mat image;
  if(isColor)
    image = loadImage(imageName, COLOR_IMAGE);
  else
    image = loadImage(imageName, GRAYSCALE_IMAGE);

  Mat resultImage = Mat::zeros(height, width, image.type());

  double resizeX = (double)width/(double)image.cols;
  double resizeY = (double)height/(double)image.rows;

  for (int i = 0; i < image.rows; i++)
  {
    for (int j = 0; j < image.cols; j++)
    {
      double iResult = resizeY * i;
      double jResult = resizeX * j;
      uint xl = std::floor(jResult);
      uint xr = std::floor(jResult + 1);
      uint yt = std::floor(iResult);
      uint yb = std::floor(iResult + 1);
      uint xmid = (xl + xr)/2;
      uint ymid = (yt + yb)/2;
      if( xr <= resultImage.cols && yb <= resultImage.rows)
      {
        resultImage.at<Vec3b>(yt,xl) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(yb,xl) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(yt,xr) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(yb,xr) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(yt,xmid) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(yb,xmid) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(ymid,xr) = image.at<Vec3b>(i, j);
        resultImage.at<Vec3b>(ymid,xl) = image.at<Vec3b>(i, j);
      }
    }
  }
  return resultImage;
}

/**
* Belnds two images into one image witha given blend ratio.
*
* @param the name of the two images, the blending ration of the
*        first to the second and whether interpolation to be used or not and
*        and the color mode.
*/
void blendImages(string imageName1, string imageName2, double ratio,
                  bool interpolated, bool isColor)
{
  Mat image1, image2;
  if(isColor)
    image1 = loadImage(imageName1, COLOR_IMAGE);
  else
    image1 = loadImage(imageName1, GRAYSCALE_IMAGE);

  Mat resultImage = Mat::zeros(image1.rows, image1.cols, image1.type());

  if(interpolated)
  {
    if(isColor)
      resize(loadImage(imageName2, COLOR_IMAGE), image2, image1.size());
    else
      resize(loadImage(imageName2, GRAYSCALE_IMAGE), image2, image1.size());
  }
  else
    image2 = resizeImage(imageName2, image1.cols, image1.rows, isColor);

  for (int i = 0; i < image1.cols; i++)
  {
    for (int j = 0; j < image1.rows; j++)
    {
      Vec3b color1 = image1.at<Vec3b>(Point(i,j));
      Vec3b color2 = image2.at<Vec3b>(Point(i,j));

      color1[0] = color1[0]*ratio + color2[0]*(1 - ratio);
      color1[1] = color1[1]*ratio + color2[1]*(1 - ratio);
      color1[2] = color1[2]*ratio + color2[2]*(1 - ratio);

      resultImage.at<Vec3b>(Point(i,j)) = saturatePixel(color1);
    }
  }
  showImage("Blended", resultImage);
}

/**
* Shears an image to get its front view using affine transformation.
* http://www.geom.uiuc.edu/docs/reference/CRC-formulas/node15.html
*
*  @param  the name of the images and the shear factor in the x and y.
*/
void affineFrontView(string imageName, float Kx, float Ky)
{
  Mat image = loadImage(imageName, GRAYSCALE_IMAGE);

  Mat resultImage = Mat::zeros(image.rows, image.cols, image.type());

  for(int i = 0; i < resultImage.rows; i++)
  {
    for(int j = 0; j < resultImage.cols; j++)
    {
      int iResult = i + j * Ky;
      int jResult = j + i * Kx;

      if( iResult >= 0 && iResult <= image.rows && jResult >= 0 && jResult <= image.cols )
      {
        resultImage.at<uchar>(i,j) = image.at<uchar>(iResult, jResult);
      }
    }
  }
  imwrite( "newL3.jpg", resultImage );
  showImage("Front View", resultImage);
}

/**
* Shears an image to get its front view using homography transformation.
*
* @param  the name of the images.
*/
void homographyFrontView(string imageName, bool isColor)
{
  Mat lambda( 2, 4, CV_32FC1 );
  Mat resultImage, image;
  if(isColor)
    image = loadImage(imageName, COLOR_IMAGE);
  else
    image = loadImage(imageName, GRAYSCALE_IMAGE);

  lambda = Mat::zeros( image.rows, image.cols, image.type() );

  outputQuad[0] = Point2f( 0,0 );
  outputQuad[1] = Point2f( image.cols-1,0);
  outputQuad[2] = Point2f( image.cols-1,image.rows-1);
  outputQuad[3] = Point2f( 0,image.rows-1  );

  lambda = getPerspectiveTransform( inputQuad, outputQuad );
  warpPerspective(image,resultImage,lambda,resultImage.size() );

  showImage("Front View", resultImage);
}
